<!-- This is Comment Test -->
<?php global $reduxgv; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <title><?php bloginfo('name'); ?></title>
    
    <!-- Loading Bootstrap -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Loading Template CSS -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style-magnific-popup.css" rel="stylesheet">
    
    <!-- Loading Layer Slider -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/layerslider/css/layerslider.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/fonts.css" rel="stylesheet">
    
    <!-- Google Fonts -->
    <link href='../../../../fonts.googleapis.com/css-family=Roboto-400,300,500,700,100.htm' rel='stylesheet' type='text/css'>
    <link href='../../../../fonts.googleapis.com/css-family=Open+Sans-400,300,300italic,400italic,600,600italic,700.htm' rel='stylesheet' type='text/css'>

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="<?php echo esc_url(get_template_directory_uri()); ?>/images/favicon.ico.htm">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
    <!--headerIncludes-->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/style.css" />
    <?php wp_head(); ?>
</head>
<body>
    
    <!--begin loader -->
    <div id="loader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--end loader -->
    
    <!--begin header -->
    <header class="header">

        <!--begin nav -->
        <nav class="navbar navbar-default navbar-fixed-top">
            
            <!--begin container -->
            <div class="container">
        
                <!--begin navbar -->
                <div class="navbar-header">
                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                                                    
                    <a href="<?php echo esc_url(get_template_directory_uri()); ?>
					
					/index.html" class="navbar-brand brand scrool"><img width="90" src="<?php $logo = $reduxgv['logo']['url']; echo $logo; ?>" alt="" /></a>
					
					
                </div>
                        
                <div id="navbar-collapse-02" class="collapse navbar-collapse">
				
				
					<?php 
							wp_nav_menu(
								array(
									'theme_location' => 'main_menu', 
									'menu_class' => 'nav navbar-nav navbar-right',
									)); 
					?>
		
                </div>
                <!--end navbar -->
                                    
            </div>
    		<!--end container -->
            
        </nav>
    	<!--end nav -->
        
    </header>
    <!--end header -->
        
    <!--begin home_wrapper -->
    <section id="home_wrapper" class="home-wrapper">
        
        <!--begin container-->
        <div class="container"> 
            
            <!--begin row-->
            <div class="row">
                    
                <!--begin col-md-12-->
                <div class="col-md-12">
                            
                    <!--begin layerslider -->
                    <div id="layerslider" class="layerslider editContent" style="width:1140px;height:525px;max-width: 1140px; margin:0 auto;">
                                                            
                        <!--begin ls-slide -->
						
						<?php
							global $post;
							$args = array( 'posts_per_page' => 20, 'post_type'=> 'slider' );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) : setup_postdata($post); 

						?>
						
                        <div class="ls-slide" data-ls="slidedelay:8000;transition2d:21,105;timeshift:-1000;">
                            
                            <img class="ls-l" style="top:180px;left:1px;white-space: nowrap;" data-ls="offsetxin:0; offsetyin:100; durationin:2000; delayin:500; offsetxout:-100; offsetyout:50; durationout:2000;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls2a.png" alt="">
                            
                            <img class="ls-l" style="top:150px;left:50px;white-space: nowrap;" data-ls="durationin:1500; delayin:50; scalexin:0.8; scaleyin:0.8; scalexout:0.8; scaleyout:0.8;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls1a.png" alt="">
                            
                            <img class="ls-l" style="top:340px;left:120px;white-space: nowrap;" data-ls="durationin:1500; delayin:1000; offsetxin: left;
                                offsetxout: right;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls3a.png" alt="">
                            
                            <img class="ls-l" style="top:245px;left:415px;white-space: nowrap;" data-ls="durationin:1500;delayin:1500;scalexin:0.8;scaleyin:0.8;scalexout:0.8;scaleyout:0.8;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls4a.png" alt="">
                            
                            <img class="ls-l" style="top:215px;left:215px;white-space: nowrap;" data-ls="offsetxin:0; durationin:1500; delayin:2000; rotateyin:90; transformoriginin:right 50% 0; offsetxout:0; durationout:1500; rotateyout:-90; transformoriginout:right 50% 0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls5a.png" alt="">
                            
                            <img class="ls-l" style="top:55px;left:75px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls6a.png" alt="">
                                            
                            <h1 class="ls-l" style="top:120px;left:590px;font-weight: 700;font-size:36px;line-height:42px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:0;offsetyin: top;delayin:3500;offsetxout:-50;durationout:3000;">BTG Business & Finance Ltd<br>Launch/Join Campaign</h1>
                            
                            <p class="ls-l" style="top:225px;left:590px;font-weight: 400;font-size:19px; line-height:32px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:50;delayin:4500;skewxin:-60;offsetxout:-50;durationout:1000;skewxout:-60;">
                                Design and style should always work toward making you look<br>
                                good and feel good - without a lot of efforts - so you always<br>
                                can get on with the things that truly matter.
                            </p>
                            
                            <a href="index.htm#services" class="ls-l scrool" style="top:350px; left:590px;" data-ls="offsetxin:0;durationin:4000;delayin:5500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:0;durationout:1000;rotatexout:90;transformoriginout:50% bottom 0;">
                                <span style="background-color: #fff; border: 1px solid #fff; color:#228798; display:inline-block;font-weight:700; font-size:14px; line-height:15px; vertical-align:top; padding:16px 30px;margin:0 0 20px 0;-webkit-border-radius: 5px 5px;-moz-border-radius: 5px 5px;border-radius: 5px 5px;white-space: nowrap;">
                                    DISCOVER MORE
                                </span>
                            </a>
                                       
                        </div>
						<?php endforeach; ?> 
						
						
						
                        <!--end ls-slide -->
                                                
                        <!--begin ls-slide -->
                        <div class="ls-slide" data-ls="slidedelay:8000;transition2d:21,105;timeshift:-1000;">
                            
                            <h1 class="ls-l" style="top:115px;left:1px;font-weight: 700;font-size:36px;line-height:42px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:0;offsetyin: top;delayin:500;offsetxout:-50;durationout:3000;">BTG Business & Finance Ltd<br>Launch/Join Campaign</h1>
                            
                            
                            <p class="ls-l" style="top:215px;left:0px;font-weight: 400;font-size:19px; line-height:32px;color:#ffffff;white-space: nowrap;" data-ls="offsetxin:50;delayin:1500;skewxin:-60;offsetxout:-50;durationout:1000;skewxout:-60;">
                                Design and style should always work toward making you look<br>
                                good and feel good - without a lot of efforts - so you always<br>
                                can get on with the things that truly matter.
                            </p>
                            
                            <a href="index.htm#services" class="ls-l scrool" style="top:340px; left:0;" data-ls="offsetxin:0;durationin:2500;delayin:2500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:0;durationout:1000;rotatexout:90;transformoriginout:50% bottom 0;">
                                <span style="background-color: #fff; border: 1px solid #fff; color:#228798; display:inline-block;font-weight:700; font-size:14px; line-height:15px; vertical-align:top; padding:16px 30px;margin:0 0 20px 0;-webkit-border-radius: 5px 5px;-moz-border-radius: 5px 5px;border-radius: 5px 5px;white-space: nowrap;">
                                    TRY IT FOR FREE
                                </span>
                            </a>
                            
                            <img class="ls-l" style="top:210px;left:650px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls2b.png" alt="">
                            
                            <img class="ls-l" style="top:90px;left:675px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls3b.png" alt="">
                            
                            <img class="ls-l" style="top:60px;left:840px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls4b.png" alt="">
                            
                            <img class="ls-l" style="top:68px;left:938px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls5b.png" alt="">
                            
                            <img class="ls-l" style="top:128px;left:1015px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls6b.png" alt="">

                            <img class="ls-l" style="top:257px;left:1040px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls7b.png" alt="">

                            <img class="ls-l" style="top:369px;left:1030px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls8b.png" alt="">

                            <img class="ls-l" style="top:375px;left:925px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls9b.png" alt="">

                            <img class="ls-l" style="top:378px;left:800px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls10b.png" alt="">

                            <img class="ls-l" style="top:322px;left:659px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:3500;easingin:linear;scalexin:0;scaleyin:0;offsetxout:0;durationout:2000;easingout:linear;scalexout:0;scaleyout:0;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls11b.png" alt="">

                            <img class="ls-l" style="top:172px;left:755px;white-space: nowrap;" data-ls="durationin:2000; delayin:2000; scalexin:0.8; scaleyin:0.8; scalexout:0.8; scaleyout:0.8;" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ls1b.png" alt="">
                            
                                       
                        </div>
                        <!--end ls-slide -->
                        
                    </div>
                    <!--end layerslider -->
                            
                </div>
                <!--end col-md-12-->
                
            </div>
            <!--end row-->
              
        </div>
        <!--end container-->
        
    </section>
    <!--end home_wrapper -->
    
    <!--begin services-->
      
   

    <!--begin section-white -->
    <section class="section-white medium-padding" id="services">
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row">
                       <!--begin col-md-10-->
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <h2 class="section-title"><?php $finding = $reduxgv['finding']; echo $finding;?></h2>
                        
                        <div class="separator_wrapper">
                            <i class="icon icon-star-two red"></i>
                        </div>
                
                      <p><?php $variation = $reduxgv['variation']; echo $variation;?></p>
                    </div>
                    <!--end col-md-10-->
                <!--begin col-md-6-->
                <div class="col-md-6 margin-top-30 margin-bottom-30">
                
                    <img src="<?php $leftbg_icon = $reduxgv['leftbg_icon']['url']; echo $leftbg_icon; ?>" class="width-100" alt="imac">
                       
                </div>
                <!--end col-sm-6-->
                
                <!--begin col-md-6-->
				
                <div class="col-md-6 margin-top-50 margin-bottom-30">
				
                
                    <h3 class="medium-title"><?php $market = $reduxgv['market']; echo $market;?></h3>
                    
                    <p> <?php $dedicated =$reduxgv['dedicated']; echo $dedicated;?>
					</p>
					
                       <h3 class="medium-title"> <?php $help = $reduxgv['help']; echo $help;?></h3>
					   
					   
					   
                    <ul class="features-list">
					
					<?php	
						global $post;
						$args = array( 'posts_per_page' => 20, 'post_type'=> 'righttopicon' );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) : setup_postdata($post); 

					?>
					
                        <li><img src="<?php $promo_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'promo_img' ); echo $promo_img[0];
					?>" alt="picture"> 
					<?php the_excerpt(); ?></li>
						
						
                    <?php endforeach; ?> 
						
						
                    </ul>
					
					
					
					
                    
                </div>
				
				
				
				
                <!--end col-sm-6-->
            
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
    <!--end section-white-->
     
    
    
    <!--begin section-grey-->
    <div class="section-grey sponsors-padding">
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row text-center">
            
                <!--begin col-sm-12-->
				
				
                <div class="col-sm-12 sponsors">
				
                    <img src="<?php $envato = $reduxgv['envato']['url']; echo $envato; ?>" class="sponsor" alt="image">
					
                    <img src="<?php $dribble = $reduxgv['dribble']['url']; echo $dribble; ?>" class="sponsor" alt="image">
					
                    <img src="<?php $behance = $reduxgv['behance']['url']; echo $behance; ?>" class="sponsor" alt="image">
					
                    <img src="<?php $wordpress = $reduxgv['wordpress']['url']; echo $wordpress; ?>" class="sponsor" alt="image">
					
                    <img src="<?php $linkdin = $reduxgv['linkdin']['url']; echo $linkdin; ?>" class="sponsor" alt="image">
					
                </div>
				
				
				
                <!--end col-sm-12-->
                
            </div>
            <!--end row-->
            
        </div>
        <!--end container-->
    
    </div> 
    <!--end section-grey-->

    <!--begin team -->
    
    <!--begin section-white -->
    <section class="section-white medium-padding" id="pro">
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row">
                       <!--begin col-md-10-->
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <h2 class="section-title"> <?php $join = $reduxgv['join']; echo $join;?></h2>
                        
                        <div class="separator_wrapper">
                            <i class="icon icon-star-two red"></i>
                        </div>
						<p> <?php $passages = $reduxgv['passages']; echo $passages;?></p>
                      
                    </div>
                    <!--end col-md-10-->
                <!--begin col-md-6-->
                <div class="col-md-6 margin-top-35 margin-bottom-30">
                    
                     <h3 class="medium-title"> <?php $helpus = $reduxgv['helpus']; echo $helpus;?></h3>
					 
                    <ul class="features-list">
					
					
						<?php
							global $post;
							$args = array( 'posts_per_page' => 20, 'post_type'=> 'leftarrowicon' );
							$myposts = get_posts( $args );
							foreach( $myposts as $post ) : setup_postdata($post); 

						?>
						
						<li>
							<img src="<?php $fetuar_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fetuar_img' ); echo $fetuar_img[0];
							?>" alt="icon">
							<?php the_excerpt(); ?>
						</li>
						<?php endforeach; ?> 
                    
                      
                    </ul>
                    
                </div>
                <!--end col-sm-6-->
            
                <!--begin col-md-6-->
                <div class="col-md-6 margin-top-30 margin-bottom-30">
                
                    <iframe src="http://player.vimeo.com/video/125804170?title=0&amp;byline=0&amp;portrait=0\n'http://player.vimeo.com/video/125804170?title=0&amp;byline=0&amp;portrait=0'" height="312" width="555"></iframe>
                       
                </div>
                <!--end col-sm-6-->
                
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>


    
    
    
    
    <!--begin section-dark -->
    <section class="section-dark medium-padding">
        
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row">
            
                <!--begin col-md-6-->
                <div class="col-md-6 margin-top-30 margin-bottom-30">
                
                    <img src="<?php $leftdownbg_icon = $reduxgv['leftdownbg_icon']['url']; echo $leftdownbg_icon; ?>" class="width-100" alt="imac">
                       
                </div>
                <!--end col-sm-6-->
                
                <!--begin col-md-6-->
				
				<?php
						global $post;
						$args = array( 'posts_per_page' => 20, 'post_type'=> 'purposepost' );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) : setup_postdata($post); 

					?>
                <div class="col-md-6 margin-top-50 margin-bottom-30">
                
                    <h3 class="medium-title white"><?php the_title(); ?></h3>
                    
                    <p class="white"><?php the_excerpt(); ?></p>
                    
                   

				   <ul class="features-list-dark">
                        <li class="white"><i class="icon icon-check-mark"></i><?php echo  get_post_meta(get_the_ID(),'1s_text', true ) ?></li>
                        <li class="white"><i class="icon icon-check-mark"></i><?php echo  get_post_meta(get_the_ID(),'2nd_text', true ) ?></li>
                    </ul>
					
					

                    <a href="index.htm#" class="btn btn-lg btn-blue small"><?php echo  get_post_meta(get_the_ID(),'discover', true ) ?></a>
					
					
					
					
					
                    
                </div>
				<?php endforeach; ?> 
                <!--end col-sm-6-->
            
            </div>
            <!--end row-->
    
        </div>
        <!--end container-->
    
    </section>
    <!--end section-dark-->
    
    <!--begin story-->
    <section class="section-grey small-padding-bottom story" id="features">
        
        <!--begin container-->
        <div class="container">
            
            <!--begin row-->
            <div class="row margin-bottom-50">
            
                <!--begin col-md-12-->
                <div class="col-md-12 text-center">
                    <h2 class="section-title"> <?php $offer = $reduxgv['offer']; echo $offer;?></h2>
                    
                    <div class="separator_wrapper">
                        <i class="icon icon-star-two red"></i>
                    </div>
            
                    <p class="section-subtitle"><?php $majority = $reduxgv['majority']; echo $majority;?></p>
                </div>
                <!--end col-md-12-->
                
            </div>
            <!--end row-->
        
            <!--begin row-->
			
            <div class="row">
			
			
				<?php
						global $post;
						$args = array( 'posts_per_page' => 20, 'post_type'=> 'img_gallery' );
						$myposts = get_posts( $args );
						foreach( $myposts as $post ) : setup_postdata($post); 

					?>
                <div class="col-md-6 col-sm-12">
                    <div class="story-block story-<?php echo  get_post_meta(get_the_ID(),'icon', true ) ?>">
                        <div class="story-text">
                            <h4><?php the_title(); ?></h4>               
                            <p><?php the_excerpt(); ?></p>
                            
                        </div>
                        <div class="story-image">
                            <img src="<?php $icon_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'icon_img' ); echo $icon_img[0];
						?>" alt="">  
						
                        </div>
                        <span class="story-arrow"></span>
                        
                        <span class="h-line"></span>
                    </div>
                </div>
				
				<?php endforeach; ?> 
                
				
				
            </div>
          
			
			
			
            
            
            
            
            
        </div>
        <!--end container-->
    
    </section> 
    <!--end story-->
    
   

    
    <!--begin contact -->
    <section class="section-dark" id="contact">
        
        <!--begin container-->
        <div class="container">
    
            <!--begin row-->
            <div class="row margin-bottom-50">
            
                <!--begin col-md-10-->
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h2 class="section-title grey"><?php $contact = $reduxgv['contact']; echo $contact;?></h2>
                    
                    <div class="separator_wrapper_white">
                        <i class="icon icon-star-two grey"></i>
                    </div>
            
                    <p class="section-subtitle grey"><?php $alteration = $reduxgv['alteration']; echo $alteration;?></p>
                </div>
                <!--end col-md-10-->
            
            </div>
            <!--end row-->
            
            <!--begin row-->
            <div class="row">
        
                <!--begin success message -->
                <p class="contact_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
               
                    
					<?php echo do_shortcode( '[contact-form-7 id="59" title="Contact form 1"]' ); ?>
					
					
            
            </div>
            <!--end row-->
            
      </div>
      <!--end container-->
            
    </section>
    <!--end contact-->
    
    <!--begin footer -->
    <div class="footer">
            
        <!--begin container -->
        <div class="container">
        
            <!--begin row -->
            <div class="row">
            
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">
                    
                    <!--begin copyright -->
                    <div class="copyright">
                        <p><?php $id_name = $reduxgv['copyright']; echo $id_name;?></p>
                        
                    </div>
                    <!--end copyright -->
                                                    
                    <!--begin footer_social -->
                    <ul class="footer_social">
					
					
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-twitter"></i>
                            </a>
                        </li>
						
						
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-pinterest"></i>
                            </a>
                        </li>
						
						
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-facebook"></i>
                            </a>
                        </li>
						
						
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-instagram"></i>
                            </a>
                        </li>
						
						
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-skype"></i>
                            </a>
                        </li>
						
						
                        <li>
                            <a href="index.htm#">
                                <i class="icon icon-dribble"></i>
                            </a>
                        </li>
						

                    </ul>
                    <!--end footer_social -->
                    
                </div>
                <!--end col-md-6 -->
                
            </div>
            <!--end row -->
            
        </div>
        <!--end container -->
                
    </div>
    <!--end footer -->
    

    <!-- Load JS here for greater good =============================-->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.nav.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.scrollTo-min.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/SmoothScroll.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/wow.js"></script>
    
	<!-- begin layerslider script-->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/layerslider/js/greensock.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/layerslider/js/layerslider.transitions.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
    
    <!-- begin custom script-->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/custom.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/plugins.js"></script>
    
    <?php wp_footer(); ?>
</body>

sdfdsfsdf
</html>