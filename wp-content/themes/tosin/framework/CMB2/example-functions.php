<?php

add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = 'popcake';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Anather Field (optional)', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
	
	$cmb->add_field( array(
        'name'       => __( 'Description' ),
        'id'         => 'something_text',
        'type'       => 'file',
    ) 
	);
    $cmb->add_field( array(
        'name'       => __( 'Cake Menu Price ' ),
        'id'         => 'price_text',
        'type'       => 'text',
    ) 
	);



    $cmb->add_field( array(
        'name'       => __( 'Anather' ),
        'id'         => 'anather_text',
        'type'       => 'text',
    ) 
	);

 

}