<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    $opt_name = "reduxgv";
    $theme = wp_get_theme(); 
    $args = array(
        'opt_name'             => $opt_name,
        'display_name'         => $theme->get( 'Name' ),
        'display_version'      => $theme->get( 'Version' ),
        'menu_type'            => 'menu',
        'allow_sub_menu'       => false,
        'menu_title'           => __( 'Theme Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Theme Options', 'redux-framework-demo' ),
        'google_api_key'       => '',
        'google_update_weekly' => false,
        'async_typography'     => true,
        'admin_bar'            => true,
        'admin_bar_icon'       => 'dashicons-portfolio',
        'admin_bar_priority'   => 50,
        'global_variable'      => '',
        'dev_mode'             => false,
        'update_notice'        => false,
        'customizer'           => true,
		'open_expanded'     => false, 
        'page_priority'        => null,
        'page_parent'          => 'themes.php',
        'page_permissions'     => 'manage_options',
        'menu_icon'            => '',
        'last_tab'             => '',
        'page_icon'            => 'icon-themes',
        'page_slug'            => '_options',
        'save_defaults'        => true,
        'default_show'         => false,
        'default_mark'         => '',
        'show_import_export'   => false,
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        'output_tag'           => true,
        'database'             => '',
        'use_cdn'              => true,
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );
    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p> </p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );



/* -- Header section start -- */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Setting', 'redux-framework-demo' ),
        'id'         => 'Header',
		'subsection'  => false,
		'icon'  => 'el el-website',
        'fields'     => array(
			
			
			array(
				'title'     => 'Upload logo',
				'id'       	=> 'logo',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/logo.jpg'),
            ),
			
			array(
				'title'     => 'Finding',
				'id'       	=> 'finding',
				'type'     	=> 'text',
				'default'  	=> 'Finding the right cause'
            ),
			
	
			array(
				'title'     => 'Many Variation',
				'id'       	=> 'variation',
				'type'     	=> 'text',
				'default'  	=> 'There are many variations of passages of Lorem Ipsum available, but the majority
have suffered alteration, by injected humour, or new randomised words.'
            ),


			array(
				'title'     => 'Market',
				'id'       	=> 'market',
				'type'     	=> 'text',
				'default'  	=> 'Market Research & Data Analysis'
            ),
			
			array(
				'title'     => 'Dedicated',
				'id'       	=> 'dedicated',
				'type'     	=> 'text',
				'default'  	=> 'We have a dedicated team of market researchers conducting primary research and analyzing secondary market data.'
            ),
			
			array(
				'title'     => 'Helpus',
				'id'       	=> 'help',
				'type'     	=> 'text',
				'default'  	=> 'This helps us to:'
            ),
			
			
			
			array(
				'title'     => 'Lefttopicon',
				'id'       	=> 'leftbg_icon',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/pic3.png'),
            ),
			
			array(
				'title'     => 'Envato',
				'id'       	=> 'envato',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/partner2.png'),
            ),
			
			array(
				'title'     => 'Dribble',
				'id'       	=> 'dribble',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/partner1.png'),
            ),
	
			array(
				'title'     => 'Behance',
				'id'       	=> 'behance',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/partner3.png'),
            ),
			
			array(
				'title'     => 'Wordpress',
				'id'       	=> 'wordpress',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/partner5.png'),
            ),
	
			array(
				'title'     => 'Linkdin',
				'id'       	=> 'linkdin',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/partner4.png'),
            ),
			
			
			array(
				'title'     => 'Join',
				'id'       	=> 'join',
				'type'     	=> 'text',
				'default'  	=> 'Start or Join campaing'
            ),
			
	
			array(
				'title'     => 'Passages',
				'id'       	=> 'passages',
				'type'     	=> 'text',
				'default'  	=> 'There are many variations of passages of Lorem Ipsum available, but the majority
have suffered alteration, by injected humour, or new randomised words.'
            ),
				array(
				'title'     => 'Help',
				'id'       	=> 'helpus',
				'type'     	=> 'text',
				'default'  	=> 'This helps us to:'
            ),
			
			
			array(
				'title'     => 'Leftdownicon',
				'id'       	=> 'leftdownbg_icon',
				'type'     	=> 'media',
				'default'  	=> array(
								'url'   => get_template_directory_uri() . '/images/pic2.png'),
            ),
			
			array(
				'title'     => 'Offer',
				'id'       	=> 'offer',
				'type'     	=> 'text',
				'default'  	=> 'What you Offer:'
            ),
			
			
			array(
				'title'     => 'Majority',
				'id'       	=> 'majority',
				'type'     	=> 'text',
				'default'  	=> 'There are many variations of passages of Lorem Ipsum available, but the majority<br>have suffered alteration, by injected humour, or new randomised words.'
            ),
			
			
			array(
				'title'     => 'Contact',
				'id'       	=> 'contact',
				'type'     	=> 'text',
				'default'  	=> 'Contact us for More Information'
            ),
			
			
			array(
				'title'     => 'Alteration',
				'id'       	=> 'alteration',
				'type'     	=> 'text',
				'default'  	=> 'There are many variations of passages of Lorem Ipsum available, but the majority<br>have suffered alteration, by injected humour, or new randomised words.'
            ),
			
			array(
				'title'     => 'Copyright',
				'id'       	=> 'copyright',
				'type'     	=> 'text',
				'default'  	=> 'Copyright © 2016'
            ),
			
			
	
	
	
	
	
			
        )
    ) );


