<?php 

add_theme_support( 'post-thumbnails');
add_image_size( 'promo_img', 55, 55, true ); 
add_image_size( 'icon_img', 92, 92, true );
add_image_size( 'fetuar_img', 303, 220, true );

// custom post register 

add_action( 'init', 'custom_slider_post' );
function custom_slider_post() {
	register_post_type( 'slider',
		array(
			'labels' => array(
				'name' => __( 'slide' ),
				'singular_name' => __( 'slide' ),
				'add_new_item' => __( 'Add New slide' )
			),
			'public' => true,
			'supports' => array('title','editor','excerpt'),
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'rewrite'  	=> array( 'slug' => 'slide' ),
		)
	);
	
}



add_action( 'init', 'right_top_icon' );
function right_top_icon() {
	register_post_type( 'righttopicon',
		array(
			'labels' => array(
				'name' => __( 'righttopicon' ),
				'singular_name' => __( 'righttopicon' ),
				'add_new_item' => __( 'Add New righttopicon' )
			),
			'public' => true,
			'supports' => array('editor','thumbnail','excerpt'),
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'rewrite'  	=> array( 'slug' => 'righttopicon' ),
		)
	);
	
}


add_action( 'init', 'custom_img_gallery' );
function custom_img_gallery() {
	register_post_type( 'img_gallery',
		array(
			'labels' => array(
				'name' => __( 'imggallery' ),
				'singular_name' => __( 'imggallery' ),
				'add_new_item' => __( 'Add New imggallery' )
			),
			'public' => true,
			'supports' => array('title', 'editor', 'thumbnail', 'excerpt','custom-fields' ),
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'rewrite'  	=> array( 'slug' => 'imggallery' ),
		)
	);
	
}


add_action( 'init', 'left_arrow_icon' );
function left_arrow_icon() {
	register_post_type( 'leftarrowicon',
		array(
			'labels' => array(
				'name' => __( 'leftarrowicon' ),
				'singular_name' => __( 'leftarrowicon' ),
				'add_new_item' => __( 'Add New leftarrowicon' )
			),
			'public' => true,
			'supports' => array('editor','thumbnail','excerpt'),
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'rewrite'  	=> array( 'slug' => 'leftarrowicon' ),
		)
	);
	
}
add_action( 'init', 'custom_post_content' );
function custom_post_content() {
	register_post_type( 'purposepost',
		array(
			'labels' => array(
				'name' => __( 'purposepost' ),
				'singular_name' => __( 'purposepost' ),
				'add_new_item' => __( 'Add New purposepost' )
			),
			'public' => true,
			'supports' => array('title','editor','excerpt'),
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'rewrite'  	=> array( 'slug' => 'purposepost' ),
		)
	);
	
}



 // menu register
 
function top_nav () {
	register_nav_menu('main_menu', 'Main Menu');
}
add_action('init', 'top_nav');


/*** Active Redux Farmwork */

if(!class_exists("ReduxFrameworkPlugin")){
require_once(get_template_directory()."/framework/redux-framework/redux-framework.php");
require_once(get_template_directory()."/framework/redux-framework/custom-options.php");
require_once(get_template_directory()."/framework/CMB2/init.php");

}

// start cmb2

add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = 'popcake';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Anather Field (optional)', 'cmb2' ),
        'object_types'  => array( 'page','purposepost' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
	
	
	
    $cmb->add_field( array(
        'name'       => __( 'Firstcontent' ),
        'id'         => '1s_text',
        'type'       => 'text',
    ) 
	);

    $cmb->add_field( array(
        'name'       => __('Secondcontent' ),
        'id'         => '2nd_text',
        'type'       => 'text',
    ) 
	);
	
	$cmb->add_field( array(
        'name'       => __('Discover' ),
        'id'         => 'discover',
        'type'       => 'text',
    ) 
	);

 

}

?>